class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :description
      t.datetime :due_date
      t.string :owner
      t.string :assignee

      t.timestamps
    end
  end
end
