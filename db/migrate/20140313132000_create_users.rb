class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :full_name
      t.string :username
      t.string :email
      t.string :pass
      t.string :word
      t.string :password_confirmation
      t.datetime :confirmed_at
      t.string :role

      t.timestamps
    end
  end
end
