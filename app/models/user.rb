class User < ActiveRecord::Base
  attr_accessible :confirmed_at, :email, :full_name, :pass, :password_confirmation, :role, :username, :word
end
